package com.barq.abs.kafka.streams.sentimentAnalysis;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import org.json.simple.JSONObject;

import java.io.Serializable;


public class InputData implements Serializable {
    public String query;
    public String text;

    public static final Serializer<InputData> InputDataSerializer = new JsonPOJOSerializer<>();
    public static final Deserializer<InputData> InputDataDeserializer = new JsonPOJODeserializer<>(InputData.class);


    public static Serde<InputData> InputDataSerde;

    public InputData() {
        // defualt constructor
    }

    public InputData(String query, String text) {
        this.query = query;
        this.text = text;
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        json.put("query", query);
        json.put("text", text);

        return json.toString();
    }

}
