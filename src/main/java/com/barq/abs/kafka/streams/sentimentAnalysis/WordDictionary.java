package com.barq.abs.kafka.streams.sentimentAnalysis;

import org.nd4j.shade.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.URL;
import java.util.Map;


public class WordDictionary {
    public static Map<String, Integer> wordDict = null;
    private static WordDictionary DICT_INSTANCE = null;
    private static final String FILE_NAME = "wordDict.json";

    private WordDictionary() {}

    public static void contructDict() {
        if (DICT_INSTANCE == null) {
            synchronized(WordDictionary.class) {
                DICT_INSTANCE = new WordDictionary();
                File dictFile = DICT_INSTANCE.getFileFromResources(FILE_NAME);
                WordDictionary.wordDict = DICT_INSTANCE.getDict(dictFile);
            }
        }
    }

    // get file from classpath, resources folder
    private File getFileFromResources(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }

    private Map<String, Integer> getDict(File file) {

        if (file == null)
            return null;

        Map<String, Integer> map = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            map = mapper.readValue(file, Map.class);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return map;
    }

    public static void main(String[] args) {
        WordDictionary.contructDict();
        Map<String, Integer> dict = WordDictionary.wordDict;
        dict.forEach((k, v) -> System.out.println("Key: " + k + ", Value: " + v));
    }
}
